package uz.example.democrud.question;

public class Choice {

    private Integer id;
    private Integer sq_id;
    private Integer answer_index;

    public Integer getId() {
        return id;
    }

    public Choice setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getSq_id() {
        return sq_id;
    }

    public Choice setSq_id(Integer sq_id) {
        this.sq_id = sq_id;
        return this;
    }

    public Integer getAnswer_index() {
        return answer_index;
    }

    public Choice setAnswer_index(Integer answer_index) {
        this.answer_index = answer_index;
        return this;
    }
}
