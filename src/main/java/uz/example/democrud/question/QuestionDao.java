package uz.example.democrud.question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class QuestionDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void init(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    void insert(Question q) {
        String sql = "insert into question(yes, no, other, sq_id) values (?,?,?,?)";

        jdbcTemplate.update(sql, new Object[]{q.getYes(), q.getNo(), q.getOther(), q.getSq_id()});
    }

    void insertChoice(Choice c) {
        String sql = "insert into choice(answer_index, sq_id) values (?,?)";

        jdbcTemplate.update(sql, new Object[]{c.getAnswer_index(), c.getSq_id()});
    }

    List<Question> getAll() {
        String sql = "select * from question";

        return jdbcTemplate.query(sql, (rs, i) -> {
            Question question = new Question();
            question.setId(rs.getInt("id"));
            question.setYes(rs.getInt("yes"));
            question.setNo(rs.getInt("no"));
            question.setOther(rs.getInt("other"));
            question.setSq_id(rs.getInt("sq_id"));
            return question;
        });
    }

    List<Question> getById(Long id) {
        String sql = "select * from question where sq_id = ?";

        return jdbcTemplate.query(sql, new Object[]{id},  (rs, i) -> {
            Question question = new Question();
            question.setId(rs.getInt("id"));
            question.setYes(rs.getInt("yes"));
            question.setNo(rs.getInt("no"));
            question.setOther(rs.getInt("other"));
            question.setSq_id(rs.getInt("sq_id"));
            return question;
        });
    }
}
