package uz.example.democrud.question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService {

    @Autowired
    private QuestionDao questionDao;

    public String insert(Question q) {
        validateBeforeInsert(q);
        questionDao.insert(q);

        return "{\"message\":\"voted successfully!\"}";
    }

    private void validateBeforeInsert(Question q) {
        if (q.getYes() == null)
            throw new RuntimeException("Yes is must have");
        if (q.getNo() == null)
            throw new RuntimeException("No is must have");
        if (q.getOther() == null)
            throw new RuntimeException("Other is must have");
        if (q.getSq_id() == null)
            throw new RuntimeException("Sq_id is must have");
    }

    public String insertChoice(Choice c) {
        validateChoiceBeforeInsert(c);
        questionDao.insertChoice(c);

        return "{\"message\":\"voted successfully!\"}";
    }

    private void validateChoiceBeforeInsert(Choice c) {
        if (c.getAnswer_index() == null)
            throw new RuntimeException("Answer index is must have");
        if (c.getSq_id() == null)
            throw new RuntimeException("Sq_id is must have");
    }

    public List<Question> getAll() {
        return questionDao.getAll();
    }

    public List<Question> getById(Long id) { return questionDao.getById(id); }
}
