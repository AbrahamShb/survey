package uz.example.democrud.question;

public class Question {

    private Integer id;
    private Integer sq_id;
    private Integer yes;
    private Integer no;
    private Integer other;

    public Integer getId() {
        return id;
    }

    public Question setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getSq_id() {
        return sq_id;
    }

    public Question setSq_id(Integer sq_id) {
        this.sq_id = sq_id;
        return this;
    }

    public Integer getYes() {
        return yes;
    }

    public Question setYes(Integer yes) {
        this.yes = yes;
        return this;
    }

    public Integer getNo() {
        return no;
    }

    public Question setNo(Integer no) {
        this.no = no;
        return this;
    }

    public Integer getOther() {
        return other;
    }

    public Question setOther(Integer other) {
        this.other = other;
        return this;
    }
}
