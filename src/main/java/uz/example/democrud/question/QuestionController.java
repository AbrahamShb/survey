package uz.example.democrud.question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @PostMapping
    public String insert(@RequestBody Question question) {
        return questionService.insert(question);
    }

    @PostMapping("/choice")
    public String insertChoice(@RequestBody Choice choice) {
        return questionService.insertChoice(choice);
    }

    @GetMapping("/all")
    public List<Question> getAll() {
        return questionService.getAll();
    }

    @GetMapping("/all/{id}")
    public  List<Question> getById(@PathVariable Long id) { return questionService.getById(id); }
}
