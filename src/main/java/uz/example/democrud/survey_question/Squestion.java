package uz.example.democrud.survey_question;

import java.util.List;

public class Squestion {

    private Integer id;
    private String name;
    private Integer status;

    public Integer getId() {
        return id;
    }


    public Squestion setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Squestion setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public Squestion setStatus(Integer status) {
        this.status = status;
        return this;
    }
}
