package uz.example.democrud.survey_question;

public class ChoiceOptions {

    private Integer id;
    private String name;
    private Integer status;
    private String[] variants;
    private Integer sq_id;

    public Integer getId() {
        return id;
    }

    public ChoiceOptions setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ChoiceOptions setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public ChoiceOptions setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String[] getVariants() {
        return variants;
    }

    public ChoiceOptions setVariants(String[] variants) {
        this.variants = variants;
        return this;
    }

    public Integer getSq_id() {
        return sq_id;
    }

    public ChoiceOptions setSq_id(Integer sq_id) {
        this.sq_id = sq_id;
        return this;
    }
}
