package uz.example.democrud.survey_question;

public class RatingOptions {

    private Integer id;
    private String name;
    private Integer status;
    private Integer count;
    private Integer sq_id;

    public Integer getId() {
        return id;
    }

    public RatingOptions setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public RatingOptions setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public RatingOptions setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Integer getCount() {
        return count;
    }

    public RatingOptions setCount(Integer count) {
        this.count = count;
        return this;
    }

    public Integer getSq_id() {
        return sq_id;
    }

    public RatingOptions setSq_id(Integer sq_id) {
        this.sq_id = sq_id;
        return this;
    }
}
