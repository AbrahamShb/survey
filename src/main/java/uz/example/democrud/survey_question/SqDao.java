package uz.example.democrud.survey_question;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Map;

@Repository
public class SqDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void init(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    void insert(Squestion sq) {
        String sql = "insert into survey_questions(name, status) values (?,?)";

        jdbcTemplate.update(sql, new Object[]{sq.getName(), sq.getStatus()});
    }

    void insertWithChoice(ChoiceOptions c) {
        String sql = "insert into survey_questions(name, status) values (?,?);"+" insert into choice_options(variants, sq_id) values (?,?)";

        jdbcTemplate.update(sql, new Object[]{c.getName(), c.getStatus(), c.getVariants(),c.getSq_id()});
    }

    void insertWithRating(RatingOptions ro) {
        String sql = "insert into survey_questions(name, status) values (?,?);"+" insert into rating_options(count, sq_id) values (?,?)";

        jdbcTemplate.update(sql, new Object[]{ro.getName(), ro.getStatus(), ro.getCount(), ro.getSq_id()});
    }

    List<Squestion> getAll() {
        String sql = "select * from survey_questions";

        return jdbcTemplate.query(sql, (rs, i) -> {
            Squestion squestion = new Squestion();
            squestion.setId(rs.getInt("id"));
            squestion.setName(rs.getString("name"));
            squestion.setStatus(rs.getInt("status"));
            return squestion;
        });
    }

    public Map<String, Object> getRowCount() {
        String sql = "select max(id) from survey_questions";

        return  jdbcTemplate.queryForMap(sql);
    }

    List<ChoiceOptions> getListForChoice() throws SQLException {
        String sql = "select sq.name,sq.status,c.id,c.variants,c.sq_id from survey_questions sq left join choice_options c on c.sq_id=sq.id where c.sq_id=sq.id;";

        return jdbcTemplate.query(sql, (rs, i) -> {
            ChoiceOptions choiceOptions = new ChoiceOptions();
            Array array = rs.getArray("variants");
            choiceOptions.setId(rs.getInt("id"));
            choiceOptions.setName(rs.getString("name"));
            choiceOptions.setStatus(rs.getInt("status"));
            choiceOptions.setSq_id(rs.getInt("sq_id"));
            choiceOptions.setVariants((String[]) array.getArray());
            return choiceOptions;
        });
    }

    List<Squestion> getListForText() {
        String sql = "select * from survey_questions where status = 2;";

        return jdbcTemplate.query(sql, (rs, i) -> {
            Squestion squestion = new Squestion();
            squestion.setId(rs.getInt("id"));
            squestion.setName(rs.getString("name"));
            squestion.setStatus(rs.getInt("status"));
            return squestion;
        });
    }

    List<RatingOptions> getListForRating() {
        String sql = "select sq.name,sq.status,ro.id,ro.count,ro.sq_id from survey_questions sq left join rating_options ro on ro.sq_id=sq.id where sq.status=3;";

        return jdbcTemplate.query(sql, (rs, i) -> {
            RatingOptions ratingOptions = new RatingOptions();
            ratingOptions.setId(rs.getInt("id"));
            ratingOptions.setName(rs.getString("name"));
            ratingOptions.setStatus(rs.getInt("status"));
            ratingOptions.setSq_id(rs.getInt("sq_id"));
            ratingOptions.setCount(rs.getInt("count"));
            return ratingOptions;
        });
    }
}
