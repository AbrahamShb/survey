package uz.example.democrud.survey_question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/squestion")
public class SqController {

    @Autowired
    private SqService sqService;

    @PostMapping
    public String insert(@RequestBody Squestion squestion) {
        return sqService.insert(squestion);
    }

    @PostMapping(path = "/choice")
    public String insertWithChoice(@RequestBody ChoiceOptions choiceOptions) {
        return sqService.insertWithChoice(choiceOptions);
    }

    @PostMapping(path = "/rating")
    public String insertWithRating(@RequestBody RatingOptions ratingOptions) {
        return sqService.insertWithRating(ratingOptions);
    }

    @GetMapping("/all")
    public List<Squestion> getAll() {
        return sqService.getAll();
    }

    @GetMapping("/count")
    public Map<String, Object> getRowCount() {return sqService.getRowCount();}

    @GetMapping("/getlistforchoice")
    public List<ChoiceOptions> getListForChoice() throws SQLException {return sqService.getListForChoice();}

    @GetMapping("/getlistfortext")
    public List<Squestion> getListForText() { return sqService.getListForText();}

    @GetMapping("/getlistforrating")
    public List<RatingOptions> getListForRating() { return sqService.getListForRating();}
}
