package uz.example.democrud.survey_question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Service
public class SqService {

    @Autowired
    private SqDao sqDao;

    public String insert(Squestion sq) {
        validateBeforeInsert(sq);
        sqDao.insert(sq);

        return "{\"message\":\"inserted successfully!\"}";
    }

    private void validateBeforeInsert(Squestion sq) {
        if (sq.getName() == null)
            throw new RuntimeException("Name is must have");
        if (sq.getStatus() == null)
            throw new RuntimeException("Status is must have");
    }

    public String insertWithChoice(ChoiceOptions c) {
        validateBeforeInsertWithCoice(c);
        sqDao.insertWithChoice(c);

        return "{\"message\":\"inserted successfully!\"}";
    }

    private void validateBeforeInsertWithCoice(ChoiceOptions c) {
        if (c.getName() == null)
            throw new RuntimeException("Name is must have");
        if (c.getStatus() == null)
            throw new RuntimeException("Status is must have");
        if (c.getVariants() == null)
            throw new RuntimeException("Variant is must have");
        if (c.getSq_id() == null)
            throw new RuntimeException("Sq_id is must have");
    }

    public String insertWithRating(RatingOptions ro) {
        validateBeforeInsertWithRating(ro);
        sqDao.insertWithRating(ro);

        return "{\"message\":\"inserted successfully!\"}";
    }

    private void validateBeforeInsertWithRating(RatingOptions ro) {
        if (ro.getName() == null)
            throw new RuntimeException("Name is must have");
        if (ro.getStatus() == null)
            throw new RuntimeException("Status is must have");
        if (ro.getCount() == null)
            throw new RuntimeException("Count is must have");
        if (ro.getSq_id() == null)
            throw new RuntimeException("Sq_id is must have");
    }

    public List<Squestion> getAll() {
        return sqDao.getAll();
    }

    public Map<String, Object> getRowCount() {return sqDao.getRowCount();}

    public List<ChoiceOptions> getListForChoice() throws SQLException { return sqDao.getListForChoice();}

    public List<Squestion> getListForText() {return sqDao.getListForText();}

    public List<RatingOptions> getListForRating() {return sqDao.getListForRating();}
}
