package uz.example.democrud.rating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingService {

    @Autowired
    private RatingDao ratingDao;

    public String insert(Rating r) {
        validateBeforeInsert(r);
        ratingDao.insert(r);

        return "{\"message\":\"ok\"}";
    }

    private void validateBeforeInsert(Rating r) {
        if (r.getCount() == null)
            throw new RuntimeException("Count is must have");
        if (r.getAllcount() == null)
            throw new RuntimeException("AllCount is must have");
        if (r.getSq_id() == null)
            throw new RuntimeException("Sq_id is must have");
    }

    public List<Rating> getAll() {
        return ratingDao.getAll();
    }
}
