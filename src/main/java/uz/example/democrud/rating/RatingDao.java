package uz.example.democrud.rating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class RatingDao  {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void init(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    void insert(Rating r) {
        String sql = "insert into rating(count, allcount, sq_id) values (?,?,?)";

        jdbcTemplate.update(sql, new Object[]{r.getCount(), r.getAllcount(), r.getSq_id()});
    }

    List<Rating> getAll() {
        String sql = "select * from rating";

        return jdbcTemplate.query(sql, (rs, i) -> {
            Rating rating = new Rating();
            rating.setId(rs.getInt("id"));
            rating.setCount(rs.getInt("count"));
            rating.setAllcount(rs.getInt("allcount"));
            rating.setSq_id(rs.getInt("sq_id"));
            return rating;
        });
    }
}
