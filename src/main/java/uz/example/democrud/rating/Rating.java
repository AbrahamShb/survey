package uz.example.democrud.rating;

public class Rating {

    private Integer id;
    private Integer count;
    private Integer allcount;
    private Integer sq_id;

    public Integer getId() {
        return id;
    }

    public Rating setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getCount() {
        return count;
    }

    public Rating setCount(Integer count) {
        this.count = count;
        return this;
    }

    public Integer getAllcount() {
        return allcount;
    }

    public Rating setAllcount(Integer allcount) {
        this.allcount = allcount;
        return this;
    }

    public Integer getSq_id() {
        return sq_id;
    }

    public Rating setSq_id(Integer sq_id) {
        this.sq_id = sq_id;
        return this;
    }
}
