package uz.example.democrud.rating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rating")
public class RatingController {

    @Autowired
    private RatingService ratingService;

    @PostMapping
    public String insert(@RequestBody Rating rating) {
        return ratingService.insert(rating);
    }

    @GetMapping("/all")
    public List<Rating> getAll() {
        return ratingService.getAll();
    }
}
