package uz.example.democrud.text;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TextService {

    @Autowired
    private TextDao textDao;

    public String insert(Text t) {
        validateBeforeInsert(t);
        textDao.insert(t);

        return "{\"message\":\"ok\"}";
    }

    private void validateBeforeInsert(Text t) {
        if (t.getName() == null)
            throw new RuntimeException("Name is must have");
        if (t.getSq_id() == null)
            throw new RuntimeException("Sq_id is must have");
    }

    public List<Text> getAll() {
        return textDao.getAll();
    }

    public List<Text> getById(Long id) { return textDao.getById(id);}
}
