package uz.example.democrud.text;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class TextDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void init(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    void insert(Text t) {
        String sql = "insert into text(name, sq_id) values (?,?)";

        jdbcTemplate.update(sql, new Object[]{t.getName(), t.getSq_id()});
    }

    List<Text> getAll() {
        String sql = "select * from text";

        return jdbcTemplate.query(sql, (rs, i) -> {
            Text text = new Text();
            text.setId(rs.getInt("id"));
            text.setName(rs.getString("name"));
            text.setSq_id(rs.getInt("sq_id"));
            return text;
        });
    }

    List<Text> getById(Long id) {
        String sql = "select * from text where sq_id = ?";

        return jdbcTemplate.query(sql, new Object[]{id}, (rs, i) -> {
            Text text = new Text();
            text.setId(rs.getInt("id"));
            text.setName(rs.getString("name"));
            text.setSq_id(rs.getInt("sq_id"));
            return text;
        });
    }
}
