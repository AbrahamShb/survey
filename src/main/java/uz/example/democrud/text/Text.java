package uz.example.democrud.text;

public class Text {

    private Integer id;
    private String name;
    private Integer sq_id;

    public Integer getId() {
        return id;
    }

    public Text setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Text setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getSq_id() {
        return sq_id;
    }

    public Text setSq_id(Integer sq_id) {
        this.sq_id = sq_id;
        return this;
    }
}
