package uz.example.democrud.text;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/text")
public class TextController {

    @Autowired
    private TextService textService;

    @PostMapping
    public String insert(@RequestBody Text text) {
        return textService.insert(text);
    }

    @GetMapping("/all")
    public List<Text> getAll() {
        return textService.getAll();
    }

    @GetMapping("/all/{id}")
    public List<Text> getById(@PathVariable Long id) { return  textService.getById(id);}
}
