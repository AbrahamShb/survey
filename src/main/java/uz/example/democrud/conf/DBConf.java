package uz.example.democrud.conf;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
public class DBConf {
    @Autowired
    private Environment env;

    @Bean
    DataSource ds() {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(env.getProperty("spring.datasource.dbcp2.driver-class-name"));
        ds.setUrl(env.getProperty("spring.datasource.dbcp2.url"));
        ds.setUsername(env.getProperty("spring.datasource.dbcp2.username"));
        ds.setPassword(env.getProperty("spring.datasource.dbcp2.password"));
        ds.setInitialSize(Integer.parseInt(env.getProperty("spring.datasource.dbcp2.initial-size")));
        ds.setMaxTotal(Integer.parseInt(env.getProperty("spring.datasource.dbcp2.max-total")));
        ds.setMaxIdle(Integer.parseInt(env.getProperty("spring.datasource.dbcp2.max-idle")));
        return ds;
    }

    @Autowired
    @Bean
    DataSourceTransactionManager tx(DataSource ds) {
        return new DataSourceTransactionManager(ds);
    }
}
