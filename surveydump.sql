PGDMP     +        	        	    w            survey    9.6.12    9.6.12 $    p           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            q           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            r           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            s           1262    16454    survey    DATABASE     �   CREATE DATABASE survey WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE survey;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            t           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12387    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            u           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16501    question    TABLE     �   CREATE TABLE public.question (
    id integer NOT NULL,
    yes integer,
    no integer,
    other integer,
    sq_id integer NOT NULL
);
    DROP TABLE public.question;
       public         postgres    false    3            �            1259    16499    question_id_seq    SEQUENCE     x   CREATE SEQUENCE public.question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.question_id_seq;
       public       postgres    false    3    188            v           0    0    question_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.question_id_seq OWNED BY public.question.id;
            public       postgres    false    187            �            1259    16521    rating    TABLE     }   CREATE TABLE public.rating (
    id integer NOT NULL,
    count integer,
    sq_id integer NOT NULL,
    allcount integer
);
    DROP TABLE public.rating;
       public         postgres    false    3            �            1259    16519    rating_id_seq    SEQUENCE     v   CREATE SEQUENCE public.rating_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.rating_id_seq;
       public       postgres    false    3    192            w           0    0    rating_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.rating_id_seq OWNED BY public.rating.id;
            public       postgres    false    191            �            1259    16490    survey_questions    TABLE     Z   CREATE TABLE public.survey_questions (
    id integer NOT NULL,
    name text NOT NULL
);
 $   DROP TABLE public.survey_questions;
       public         postgres    false    3            �            1259    16488    survey_questions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.survey_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.survey_questions_id_seq;
       public       postgres    false    3    186            x           0    0    survey_questions_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.survey_questions_id_seq OWNED BY public.survey_questions.id;
            public       postgres    false    185            �            1259    16509    text    TABLE     j   CREATE TABLE public.text (
    id integer NOT NULL,
    name text NOT NULL,
    sq_id integer NOT NULL
);
    DROP TABLE public.text;
       public         postgres    false    3            �            1259    16507    text_id_seq    SEQUENCE     t   CREATE SEQUENCE public.text_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.text_id_seq;
       public       postgres    false    190    3            y           0    0    text_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.text_id_seq OWNED BY public.text.id;
            public       postgres    false    189            �           2604    16504    question id    DEFAULT     j   ALTER TABLE ONLY public.question ALTER COLUMN id SET DEFAULT nextval('public.question_id_seq'::regclass);
 :   ALTER TABLE public.question ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    188    187    188            �           2604    16524 	   rating id    DEFAULT     f   ALTER TABLE ONLY public.rating ALTER COLUMN id SET DEFAULT nextval('public.rating_id_seq'::regclass);
 8   ALTER TABLE public.rating ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    191    192    192            �           2604    16493    survey_questions id    DEFAULT     z   ALTER TABLE ONLY public.survey_questions ALTER COLUMN id SET DEFAULT nextval('public.survey_questions_id_seq'::regclass);
 B   ALTER TABLE public.survey_questions ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    186    185    186            �           2604    16512    text id    DEFAULT     b   ALTER TABLE ONLY public.text ALTER COLUMN id SET DEFAULT nextval('public.text_id_seq'::regclass);
 6   ALTER TABLE public.text ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    190    189    190            i          0    16501    question 
   TABLE DATA               =   COPY public.question (id, yes, no, other, sq_id) FROM stdin;
    public       postgres    false    188   |"       z           0    0    question_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.question_id_seq', 14, true);
            public       postgres    false    187            m          0    16521    rating 
   TABLE DATA               <   COPY public.rating (id, count, sq_id, allcount) FROM stdin;
    public       postgres    false    192   �"       {           0    0    rating_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.rating_id_seq', 7, true);
            public       postgres    false    191            g          0    16490    survey_questions 
   TABLE DATA               4   COPY public.survey_questions (id, name) FROM stdin;
    public       postgres    false    186   #       |           0    0    survey_questions_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.survey_questions_id_seq', 2, true);
            public       postgres    false    185            k          0    16509    text 
   TABLE DATA               /   COPY public.text (id, name, sq_id) FROM stdin;
    public       postgres    false    190   �#       }           0    0    text_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.text_id_seq', 5, true);
            public       postgres    false    189            �           2606    16506    question question_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.question DROP CONSTRAINT question_pkey;
       public         postgres    false    188    188            �           2606    16526    rating rating_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.rating DROP CONSTRAINT rating_pkey;
       public         postgres    false    192    192            �           2606    16498 &   survey_questions survey_questions_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.survey_questions
    ADD CONSTRAINT survey_questions_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.survey_questions DROP CONSTRAINT survey_questions_pkey;
       public         postgres    false    186    186            �           2606    16517    text text_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.text
    ADD CONSTRAINT text_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.text DROP CONSTRAINT text_pkey;
       public         postgres    false    190    190            i   F   x�3�4�4 BC.#d��L�" �)\�.fgY@YF\�PuF\�pACC�j8�i�o��(F��� "��      m   4   x�3�4�4�4�2�4�Ɯ��F��\&P	SN3 m�eT`�͡
c���� ���      g   �   x�N��0��W����X@�2�iCs$��I%گǝ|ݝ�Vz��k&�݂>2]�Շ��T� g,��T��\�+t�h�9��
7�L}�5nЎ�k��ww#y��c"	�Ă<�P�NF��q�ɂ)dx�G,Ae.	��<�ι?(p?R      k   �   x�M�;�0��z}���4H	p�P�p�1X�8�U�@�Ӄ;��/}3��	�&p(^	B���̂\�V�����b�uO��[�P���<�}tG�́������丶���GZ��4�5X��-(}e�N\wƘ/�2     